# Intalação do Gerenciador de Bots no Linux

## Baixando os arquivos

- Primeiramento você precisa ter uma conexão com internet.

- Agora você deve ter o **GIT** instalado em sua máquina.

	- Utilize esse comando em seu terminal `git clone https://bitbucket.org/SMCodesC/robot-manager_linux.git`

- Agora precisa apenas entrar no diretório baixado e digitar esse comando `./robot-manager`.

- Agora só se divertir programando **Go Codar**